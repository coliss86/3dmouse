# 3d mouse 🐭

A 3d mouse to rotate object in Fusion 360, Openscad or Cura.

Hardware features:
  - 5 switchs
  - 1 joystick
  - Handwired
  - Use a Pro Micro ATmega32U4
  - 3D printed case

![3D mouse](doc/3dmouse.jpg)

## Project structure

* **/case** - `stl`, `step` and `fusion 360` files
* **/qmk** - Firmware folder for QMK
* **/doc** - Pictures, Json files used for [keyboard-layout-editor.com](http://www.keyboard-layout-editor.com/)

## Layouts 👗👔

As the mouse can send any key, I have created 3 layouts for CAD softwares.
To switch between these layout, press the switch `Func` and the key corresponding to the software to activate it:

![Func](doc/layout-func.png)

Currently, 3 softwares are supported:
- Fusion 360 (default): ![Fusion 360](doc/layout-fusion360.png)
- Cura: ![Cura](doc/layout-cura.png)
- Openscad: ![Openscad](doc/layout-openscad.png)

See [the keymap for full details](qmk/keymaps/default/keymap.c).

Note: by default Cura and Fusion 360 do not have the same shortcuts to rotate the view: `Right click` for Cura, `Shift + middle click` for Fusion 360.
In order to have the same configuration, in Fusion 360, go to *preferences* and select `Tinkercad` in the field *Pan, Zoom, Orbits shortcuts*.

## Electronic 📟

Top removed:
![Joystick and cherry](doc/top-removed.jpg)

Here is the inside of the box:
![Handwired](doc/handwired.jpg)

The pinout of the ATmega32u4 can [be found here](https://i.pinimg.com/originals/a5/04/57/a504570b03287da7cdd419270ec49603.jpg)

## Bill of material 🧾

- [ ] 1 * ATmega 32u4
- [ ] 5 * Cherry Mx Switchs + Keycaps
- [ ] 1 * Playstation joystick

## 3D Case 🔧

I designed the case with Fusion 360. Fusion 360, STEP and STL files are provided in [the case](case/) folder.

![Case](doc/case.jpg)

### Tools / Misc

- [ ] 3d Printer + Filament
- [ ] Soldering Iron + Solder
- [ ] Multimeter
- [ ] Glue

## Credits

- [Instructable project](https://www.instructables.com/Space-Mouse-With-Arduino-Micro-Fully-Printable/)
- [Swifticons](https://www.flaticon.com/authors/swifticons)
