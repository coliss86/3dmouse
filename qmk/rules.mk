
TAP_DANCE_ENABLE = yes      # Tap dance

# voir /Users/guillaume/projets/keyboard/qmk_firmware/keyboards/edinburgh41
# ou /Users/guillaume/projets/keyboard/qmk_firmware/keyboards/splitkb/kyria/keymaps/gotham/thumbstick.c

POINTING_DEVICE_ENABLE = yes
POINTING_DEVICE_DRIVER = analog_joystick # mouse mouvement

CONSOLE_ENABLE = no # debug, use with #include "print.h", print() and uprint() with qmk toolbox,
# to keep only uprint(), add #define USER_PRINT to config.h (save space)
