/*
Copyright 2021 coliss86

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

/* key matrix size */
#define MATRIX_ROWS 1
#define MATRIX_COLS 5

#define MATRIX_ROW_PINS { D1 }
#define MATRIX_COL_PINS { D0, D4, C6, D7, E6 }

#define ANALOG_JOYSTICK_X_AXIS_PIN F7
#define ANALOG_JOYSTICK_Y_AXIS_PIN F6
#define POINTING_DEVICE_INVERT_X true

#define POINTING_DEVICE_TASK_THROTTLE_MS 30

/* default layer to activate F360, CURA, OPENSCAD */
#define DEFAULT_LAYER F360
