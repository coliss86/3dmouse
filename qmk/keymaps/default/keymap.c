/* Copyright 2021 coliss86
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H
#include "version.h"
#include "keymap_french.h"

bool mouse_move = false;

enum layers {
    F360 = 0,  // fusion 360
    CURA,      // Ultimaker Cura
    OPENSCAD,  // Openscad
    _FUNC,     // bridge to other layer
};

#ifndef DEFAULT_LAYER
#    error "DEFAULT_LAYER is not defined"
#endif

// Tap Dance declarations
enum tap_dance {
    TD_SUPER_ESC, TD_F360_C_CENTER, TD_F360_D_I
};

#define S_ESC  TD(TD_SUPER_ESC)
#define F360_C_C TD(TD_F360_C_CENTER)
#define F360_D_I TD(TD_F360_D_I)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [F360] = LAYOUT(
//       ┌────────┬────────┬────────┬────────┬────────┐
          S_ESC,   FR_L,    F360_C_C,FR_R,    F360_D_I
//                 line      |       rectangle |
//                     circle/center view   dimension/info
    ),
    [CURA] = LAYOUT(
//       ┌────────┬────────┬────────┬────────┬────────┐
          S_ESC,   FR_R,    FR_S,    FR_T,    KC_NO
//                 rotate   scale    translate
    ),
    [OPENSCAD] = LAYOUT(
//       ┌────────┬────────┬────────┬────────┬────────┐
          S_ESC,   KC_F5,   KC_F6,   KC_F7,   KC_NO
//                 preview  render   export STL
    ),
    [_FUNC] = LAYOUT(_______, DF(OPENSCAD), DF(F360), DF(CURA), QK_BOOT),
};

layer_state_t old_layer_state;

// Tap Dance definitions
typedef enum {
    TD_NONE,
    TD_UNKNOWN,
    TD_SINGLE_TAP,
    TD_SINGLE_HOLD,
    TD_DOUBLE_TAP
} td_state_t;

typedef struct {
    bool is_press_action;
    td_state_t state;
} td_tap_t;

td_state_t cur_dance(tap_dance_state_t *state) {
    if (state->count == 1) {
        if (!state->pressed) return TD_SINGLE_TAP;
        else return TD_SINGLE_HOLD;
    } else if (state->count == 2) return TD_DOUBLE_TAP;
    else return TD_UNKNOWN;
}

// Initialize tap structure associated with example tap dance key
static td_tap_t ql_tap_state = {
    .is_press_action = true,
    .state = TD_NONE
};

void td_super_esc_finished(tap_dance_state_t *state, void *user_data) {
    ql_tap_state.state = cur_dance(state);
    switch (ql_tap_state.state) {
          case TD_SINGLE_TAP:
                tap_code(KC_ESC);
                break;
          case TD_SINGLE_HOLD:
                layer_on(_FUNC);
                break;
          case TD_DOUBLE_TAP:
                tap_code16(S(FR_C));
                break;
          default:
                break;
    }
}

void td_super_esc_reset(tap_dance_state_t *state, void *user_data) {
    // If the key was held down and now is released then switch off the layer
    if (ql_tap_state.state == TD_SINGLE_HOLD) {
        layer_off(_FUNC);
    }
    ql_tap_state.state = TD_NONE;
}

void td_f360_c_center(tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        tap_code(FR_C);
    } else {
        tap_code16(KC_BTN3);
        wait_ms(200);
        tap_code16(KC_BTN3);
    }
}

tap_dance_action_t tap_dance_actions[] = {
    [TD_SUPER_ESC] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, td_super_esc_finished, td_super_esc_reset),
    [TD_F360_C_CENTER] = ACTION_TAP_DANCE_FN(td_f360_c_center),
    [TD_F360_D_I] = ACTION_TAP_DANCE_DOUBLE(FR_D, FR_I),
};

// to write a debug value to the console
//char buffer[50];
//sprintf(buffer, "ADC:%u\n", xValue);
//uprintf("%s", buffer);

void keyboard_post_init_user(void) {
    layer_on(DEFAULT_LAYER);
}

report_mouse_t pointing_device_task_user(report_mouse_t mouse_report) {
    if (!mouse_move && (mouse_report.x != 0 || mouse_report.y != 0)) {
        switch(biton32(layer_state)) {
            case F360:
            case CURA:
                mouse_report.buttons |= MOUSE_BTN2;
                break;
            case OPENSCAD:
                mouse_report.buttons |= MOUSE_BTN1;
                break;
            }
        mouse_move = true;
    }

    if (mouse_move && mouse_report.x == 0 && mouse_report.y == 0 ) {
        switch(biton32(layer_state)) {
            case F360:
            case CURA:
                mouse_report.buttons &= ~MOUSE_BTN2;
                break;
            case OPENSCAD:
                mouse_report.buttons &= ~MOUSE_BTN1;
                break;
            }
        mouse_move = false;
    }
    return mouse_report;
}
